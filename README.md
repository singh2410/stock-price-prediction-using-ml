# Stock Price Prediction Using ML 
# By- Aarush Kumar
Predicting the price of Stock price by training a model with previous datasets using machine learning techniques in Python.
Predicting how the stock market will perform is one of the most difficult things to do. There are so many factors involved in the prediction – physical factors vs. physhological, rational and irrational behaviour, etc. All these aspects combine to make share prices volatile and very difficult to predict with a high degree of accuracy.
Can we use machine learning as a game changer in this domain? Using features like the latest announcements about an organization, their quarterly revenue results, etc., machine learning techniques have the potential to unearth patterns and insights we didn’t see before, and these can be used to make unerringly accurate predictions.
So here is the project in which I implemented ML techniques to predict the stock values.
Thankyou!
